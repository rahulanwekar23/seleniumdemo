import java.util.ArrayList;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class Accessdropdown {

	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", "C:\\new_workspace\\SeleniumDay1\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.get("http://demo.guru99.com/test/newtours/register.php");
		driver.manage().window().maximize();
		
		
		Select select = new Select(driver.findElement(By.name("country")));
		
		select.selectByVisibleText("INDIA");
		Thread.sleep(3000);
		
		((JavascriptExecutor)driver).executeScript("window.open()");
		ArrayList<String> tabs= new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs.get(1));
		
		driver.get("http://output.jsbin.com/osebed/2");
		Select fruit = new Select(driver.findElement(By.id("fruits")));
		fruit.selectByIndex(0);
		fruit.selectByValue("orange");
		fruit.selectByVisibleText("Grape");
		fruit.deselectAll();
	
	}

}
