import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class HandleTable {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", "C:\\new_workspace\\SeleniumDay1\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.get("https://demoqa.com/webtables");
		driver.manage().window().maximize();
		List<WebElement> rowWithData = driver.findElements(By.xpath("//div[contains(@class,'rt-td') and text()]/ancestor::div[contains(@class,'rt-tr-group')]"));
		/*for(int i=0;i<rowWithData.size();i++){
			System.out.println("Data of Row " + (i+1) + " is:" );
			List<WebElement> colsWithData = rowWithData.get(i).findElements(By.xpath("//div[@class='rt-td'][text()]"));
			for(int j=0;j<colsWithData.size();j++){
				System.out.println("Data at cell with Row " + (i+1) + " Column " + (j+1) + ":" + colsWithData.get(j).getText());
			}
		}*/
		
		
		for (int k=1;k<=rowWithData.size();k++){
			WebElement firstName = driver.findElement(By.xpath("//div[@class='rt-tr-group'][" +k+ "]//div[@class='rt-td'][1]"));
			if(firstName.getText().equalsIgnoreCase("Alden")){
				driver.findElement(By.xpath("//div[@class='rt-tr-group'][" +k+ "]//span[@title='Delete']")).click();
				break;
			
			}
		}
	}

}
