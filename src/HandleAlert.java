import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class HandleAlert {

	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", "C:\\new_workspace\\SeleniumDay1\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.get("http://demo.guru99.com/test/delete_customer.php");
		driver.manage().window().maximize();
		
		WebElement customerIdText = driver.findElement(By.name("cusid"));
		WebElement submitBtn = driver.findElement(By.name("submit"));
		customerIdText.sendKeys("53920");
		submitBtn.click();
		
		Alert alert = driver.switchTo().alert();
		
		String alertText = alert.getText();
		System.out.println(alertText);
		Thread.sleep(2000);
		alert.accept();
		Thread.sleep(1000);
		System.out.println(alert.getText());
		alert.accept();
	}

}
