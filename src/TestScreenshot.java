import java.io.File;
import java.io.IOException;

import javax.swing.plaf.FileChooserUI;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class TestScreenshot {

	public static void main(String[] args) throws IOException {
		System.setProperty("webdriver.chrome.driver", "C:\\new_workspace\\SeleniumDay1\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.get("http://demo.guru99.com/test/delete_customer.php");
		driver.manage().window().maximize();
		
		TakesScreenshot screenshot = (TakesScreenshot) driver;
		
		File srcFile = screenshot.getScreenshotAs(OutputType.FILE);
		
		File desFile = new File("C:\\new_workspace\\SeleniumDay1\\screenshot.jpg");
		FileUtils.copyFile(srcFile, desFile);
		driver.quit();
	}

}
