import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class AccessCheckbox {

	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", "C:\\new_workspace\\SeleniumDay1\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.get("http://demo.guru99.com/test/radio.html");
		driver.manage().window().maximize();
		
		WebElement radio1 = driver.findElement(By.id("vfb-7-1"));
		WebElement radio2 = driver.findElement(By.id("vfb-7-2"));
		
		radio1.click();
		System.out.println("First radio buttion is selected");
		

		radio2.click();
		System.out.println("Second radio buttion is selected");
		Thread.sleep(2000);
		
		List<WebElement> checkboxList = driver.findElements(By.xpath("//input[@type='checkbox']"));
		for(int i=0;i<checkboxList.size();i++){
			checkboxList.get(i).click();
			Thread.sleep(2000);
			if(checkboxList.get(i).isSelected()){
				System.out.println("Checkbox "+ i+1 + " is selected");
			}
			else{
				System.out.println("Checkbox "+ i+1 + "is not selected");
			}
		}
		
		driver.quit();
	}

}
